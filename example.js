const confirmKey = require('.')
const crypto = require('crypto')

const key = crypto.randomBytes(32)
console.log(confirmKey(key))

const names = require('./names.json')
console.log(confirmKey(key, { wordlist: names }))
